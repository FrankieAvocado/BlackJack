/**
 * Created by Edmund on 11/7/2016.
 */
var chai = require("chai");
var expect = chai.expect;
var spies = require("chai-spies");
var assert = chai.assert;
var dealerService = require("../app/components/dealer/dealer-service.js");

chai.use(spies);

describe("The Dealer Service", function(){

    var fakeCardList = [
        {
           face: {
               name: "Ace",
               values: [1, 11]
           },
           suit: {
               name: "Spades",
           }
        },
        {
           face: {
               name: "Seven",
               values: [7]
           },
           suit: {
               name: "Hearts",
           }
        },
        {
            face: {
                name: "Jack",
                values: [10]
            },
            suit: {
                name: "Diamonds",
            }
        },
        {
            face: {
                name: "Two",
                values: [2]
            },
            suit: {
                name: "Clubs",
            }
        }
    ];

    var fakeDeckService = {};
    fakeDeckService.unwrapDeck = chai.spy(function(){ return fakeCardList});

    it("should start the deck with unwrapped deck.", function(){
        // arrange
        var toTest = dealerService(fakeDeckService);

        // act
        var cards = toTest.list();

        // assert
        assert.equal(fakeCardList.length, cards.length);
        expect(fakeDeckService.unwrapDeck).to.have.been.called.once();
    });

    it("should not modify the deck count when shuffled.", function(){
        // arrange
        var toTest = dealerService(fakeDeckService);

        // act
        toTest.shuffle();
        var cards = toTest.list();

        // assert
        assert.equal(fakeCardList.length, cards.length);
    });

    it("should remove the top card when drawing", function(){
        // arrange
        var startingCardCount = fakeCardList.length;
        var toTest = dealerService(fakeDeckService);

        // act
        var topCard = toTest.list()[0];
        var drawnCard = toTest.draw();

        // assert
        assert.equal(topCard.suit, drawnCard.suit);
        assert.equal(topCard.face, drawnCard.face);
        assert.equal(startingCardCount- 1, toTest.list().length);
    });
});