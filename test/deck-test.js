/**
 * Created by Edmund on 11/7/2016.
 */
var chai = require("chai");
var assert = chai.assert;
var dealerService = require("../app/components/deck/deck-service.js");

describe("The Deck Service", function(){
   it("Should contain 52 cards in a freshly unrwapped deck", function(){
       // arrange
       var toTest = dealerService();

       // act
       var cardList = toTest.unwrapDeck();

       // assert
       assert.equal(52, cardList.length);
   });
});