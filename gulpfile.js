var gulp = require("gulp");
var sass = require("gulp-sass");
var browserify = require("browserify");
var source = require("vinyl-source-stream");
var buffer = require("vinyl-buffer");
var uglify = require("gulp-uglify");
var sourcemaps = require("gulp-sourcemaps");
var stringify = require("stringify");

gulp.task("sass", [], function(){
    return gulp.src("./app/styles/main.scss")
        .pipe(sass().on("error", sass.logError))
        .pipe(gulp.dest("./app/dist"));
});

gulp.task("browserify", [], function(){

    var b = browserify({
        entries: "./app/components/main.js",
        debug: true
    });

    return b.transform(stringify,
        {
            appliesTo: {includeExtensions: [".html"]},
            minify: true
        })
        .bundle()
        .pipe(source("bundle.js"))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(uglify())
        .pipe(sourcemaps.write("./"))
        .pipe(gulp.dest("./app/dist"))

});

gulp.task("build", ["sass", "browserify"]);