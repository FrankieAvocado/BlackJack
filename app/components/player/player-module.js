/**
 * Created by lutem on 11/8/2016.
 */
var angular = require("angular");
var playerService = require("./player-service.js");
var playerController = require("./player-controller.js");
var dealerModule = require("../dealer/dealer-module.js");
var rulesModule = require("../rules/rules-module.js");

module.exports = function(){
    var playerModule =
        angular
            .module("playerModule", ["dealerModule"])
            .factory("playerService", ["dealerService", "rulesService", playerService])
            .controller("playerController", ["playerService", playerController]);

    return playerModule;
}();
