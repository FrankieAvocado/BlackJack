/**
 * Created by lutem on 11/8/2016.
 */

module.exports = function() {
    var playerService = function (dealerService, rulesService) {

        var hand = [];
        var score = 0;

        dealerService.shuffle();

        var requestCard = function(){
            var newCard = dealerService.draw();
            hand.push(newCard);
            score = rulesService.calculateScore(hand);
        };

        var showHand = function(){
            return hand;
        };

        var getScore = function(){
            return score;
        };

        var observeHouse = function(){
            dealerService.decideWhetherToDraw();
            return {
                hand: dealerService.getHouseHand(),
                score: dealerService.getHouseScore()
            };
        };

        return {
            requestCard: requestCard,
            getScore: getScore,
            showHand: showHand,
            observeHouse: observeHouse
        };

    };

    return playerService;
}();