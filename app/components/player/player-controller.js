/**
 * Created by lutem on 11/8/2016.
 */
module.exports = function() {
    var controller = function (playerService) {
        var vm = this;
        vm.currentHand = [];
        vm.score = 0;
        vm.house = {
            score: 0,
            hand: []
        };

        vm.drawCard = function(){
            playerService.requestCard();
            vm.currentHand = playerService.showHand();
            vm.score = playerService.getScore();
            vm.house = playerService.observeHouse();

        }
    };

    return controller;
}();