/**
 * Created by Edmund on 11/7/2016.
 */
var angular = require("angular");
var deckService = require("./deck-service.js");

module.exports = function(){
   var deckModule = angular
       .module("deckModule", [])
       .service("deckService", deckService);

    return deckModule;
}();