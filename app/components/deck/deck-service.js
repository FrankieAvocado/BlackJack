/**
 * Created by Edmund on 11/7/2016.
 */

module.exports = function(){

    var deckService = function(){
        var suitList = [
            {
                name: "Spades",
                color: "black",
                icon: "♠"
            },
            {
                name: "Hearts",
                color: "red",
                icon: "♥"
            },
            {
                name: "Diamonds",
                color: "red",
                icon: "♦"
            },
            {
                name: "Clubs",
                color: "black",
                icon: "♣"
            }
        ];

        var faceList = [
            {
                name: "Ace",
                values: [1, 11]
            },
            {
                name: "King",
                values:[10]
            },
            {
                name: "Queen",
                values:[10]
            },
            {
                name: "Jack",
                values:[10]
            },
            {
                name: "Ten",
                values:[10]
            },
            {
                name: "Nine",
                values:[9]
            },
            {
                name: "Eight",
                values:[8]
            },
            {
                name: "Seven",
                values:[7]
            },
            {
                name: "Six",
                values:[6]
            },
            {
                name: "Five",
                values:[5]
            },
            {
                name: "Four",
                values:[4]
            },
            {
                name: "Three",
                values:[3]
            },
            {
                name: "Two",
                values:[2]
            }
        ];

        var unwrapDeck = function(){
            var newDeck = [];

            for(var i = 0; i < suitList.length; i++){
                var suit = suitList[i];
                for(var j = 0; j < faceList.length; j++){
                    var face = faceList[j];
                    newDeck.push({
                        suit: suit,
                        face: face
                    })
                }
            }

            return newDeck;
        };

        return{
            unwrapDeck:unwrapDeck
        }
    };

    return deckService;

}();