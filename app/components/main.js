/**
 * Created by Edmund on 11/7/2016.
 */
var angular = require("angular");
var dealerModule = require("./dealer/dealer-module.js");
var cardModule = require("./card/card-module.js");
var rulesModule = require("./rules/rules-module.js");
var playerModule = require("./player/player-module.js");

angular.module("blackJackApp", ["dealerModule", "cardModule", "playerModule", "rulesModule"]);