/**
 * Created by Edmund on 11/7/2016.
 */

module.exports = function() {
    var controller = function (dealerService) {
        var vm = this;
        dealerService.shuffle();
        vm.allcards = dealerService.list();
    };

    return controller;
}();