/**
 * Created by Edmund on 11/7/2016.
 */

module.exports = function(){
    var dealerService = function(deckService, rulesService){

        var deck = deckService.unwrapDeck();
        var houseHand = [];
        var houseScore = 0;

        var list = function(){
            return deck;
        };

        var draw = function(){
            var topCard = deck.splice(0, 1)[0];
            return topCard;
        };

        var shuffle = function(){
            var timesToShuffle = Math.floor((Math.random() * 20) + 5);
            for(var i = 0; i < timesToShuffle; i++){
                deck = doTheBridgeThing(deck);

                var nextRandom = Math.floor(Math.random() * 2);
                if(nextRandom > 0){
                    deck = cutTheDeck(deck);
                }
            }
        };

        var cutTheDeck = function(toCut){
            var nextRandom = Math.floor((Math.random() * 8) + 1);
            var midPoint = (toCut.length / 2) + toCut.length % 2 - nextRandom;
            var halves = [toCut.slice(0, midPoint), toCut.slice(midPoint, toCut.length)];
            var returnMe = [];
            // could use the new spread operator here if I take the time to configure an ES6 transform in Gulp
            for(var i = 0; i < halves.length; i++){
                var thisHalf = halves[i];
                for(var j = 0; j < thisHalf.length; j++){
                    returnMe.push(thisHalf[j]);
                }
            }

            return returnMe;
        };

        var doTheBridgeThing = function(toBridge){
            var midPoint = (toBridge.length / 2) + toBridge.length % 2;
            var topHalf = toBridge.slice(0, midPoint);
            var bottomHalf = toBridge.slice(midPoint, toBridge.length);
            var returnMe = [];

            var topPos = 0;
            var bottomPos = 0;

            for(var i = 0; i < toBridge.length; i++){
                var nextRandom = Math.floor((Math.random() * 2));

                if(nextRandom > 0){
                    // progress normally
                    if(topPos >= bottomPos && bottomPos < midPoint){
                        returnMe.push(bottomHalf[bottomPos++]);
                    }
                    else{
                        returnMe.push(topHalf[topPos++]);
                    }
                }
                else{
                    if(topPos <= bottomPos && bottomPos < midPoint){
                        returnMe.push(bottomHalf[bottomPos++]);
                    }
                    else{
                        returnMe.push(topHalf[topPos++]);
                    }
                }
            }

            return returnMe;

        };

        var decideWhetherToDraw = function(){
            houseScore = rulesService.calculateScore(houseHand);
            if(houseScore < 15){
                houseHand.push(draw());
                houseScore = rulesService.calculateScore(houseHand);
            }
        };

        var getHouseScore = function(){
            return houseScore;
        };

        var getHouseHand = function(){
            return houseHand;
        };

        return{
            list: list,
            draw: draw,
            shuffle: shuffle,
            getHouseScore: getHouseScore,
            getHouseHand: getHouseHand,
            decideWhetherToDraw: decideWhetherToDraw
        }

    };

    return dealerService;
}();