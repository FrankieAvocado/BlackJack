/**
 * Created by Edmund on 11/7/2016.
 */
var angular = require("angular");
var dealerService = require("./dealer-service.js");
var dealerController = require("./dealer-controller.js");
var deckModule = require("../deck/deck-module.js");
var rulesModule = require("../rules/rules-module.js");

module.exports = function(){
    var dealerModule =
        angular
            .module("dealerModule", ["deckModule", "rulesModule"])
            .factory("dealerService", ["deckService", "rulesService", dealerService])
            .controller("dealerController", ["dealerService", dealerController]);

    return dealerModule;
}();
