/**
 * Created by lutem on 11/8/2016.
 */
var angular = require("angular");
var rulesService = require("./rules-service.js");

module.exports = function(){
    var rulesModule =
        angular
            .module("rulesModule", [])
            .factory("rulesService", [rulesService]);

    return rulesModule;
}();
