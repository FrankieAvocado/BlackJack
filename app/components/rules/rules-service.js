/**
 * Created by lutem on 11/8/2016.
 */

module.exports = function(){

    var rulesService = function(){
        var calculateScore = function(hand){
            var result = hand.map(function(card){return card.face.values[0]}).reduce(function(total, current) {return total + current}, 0);
            return result;
        };

        var checkForWin = function(userScore, dealerScore){
            var win = !checkForFailure(userScore) && userScore > dealerScore;
        };

        var checkForFailure = function(score){
            var failed = score > 21;
            return failed;
        };

        return {
            calculateScore: calculateScore
        };
    };

    return rulesService;

}();