/**
 * Created by Edmund on 11/7/2016.
 */
var angular = require("angular");
var cardDirective = require("./card-directive.js");

module.exports = function(){
    var cardModule = angular.module("cardModule", []).directive("card", cardDirective);

    return cardModule;
}();