/**
 * Created by Edmund on 11/7/2016.
 */
var cardController = require("./card-controller.js");
var cardTemplate = require("./card-template.html");

module.exports = function(){

    var cardDirective =  function(){
        return {
            restrict: "E",
            scope:{
                face: "=",
                suit: "="
            },
            template: cardTemplate,
            controller: cardController,
            controllerAs: "cardDirectiveCtrl",
            bindToController: true
        }
    };

    return cardDirective

}();